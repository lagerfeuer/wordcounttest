#!/bin/python3
from mimesis import Text
from string import punctuation
from tqdm import tqdm

FILENAME = "gen_sample.txt"
TEXT_GEN = Text('en')

pbar = tqdm(total=700*1000)

with open(FILENAME, "w+") as f:
    for i in range(700):
        for j in range(1000):
            sentence = "".join(c for c in TEXT_GEN.sentence() if c not in punctuation)
            f.write("{0}:{1} {2}\n".format(i, j, sentence))
            pbar.update(1)
pbar.close()

